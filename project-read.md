# Recipe App API Proxy
NGINX proxy app

## usage

### environment variables
 * `LISTEN_PORT` - Port to listen on (default: `8000`)
 * `APP_HOST` - Hostname of app to forward reqs to (default: `app`)
 * `APP_PORT` - Port of app to forward reqs to (default: `9000`)